import React, { useEffect, useState } from 'react';

import './index.css';
import ReactDOM from 'react-dom/client';

// import App from './App';
// import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import { Routes, Route as ReactRoute} from "react-router";
import Header from './components/Header/Header';
import Propos from './routes/Propos';
import Home from './routes/Home';
import Footer from './components/Footer/Footer';
import ContactForm from './routes/Contact/ContactForm';
import NosVoyages from './routes/NosVoyages/NosVoyages';
import GetDatas from './components/Voyage/GetDatas';

// function GetData(){
//   const [datas,setDatas]=useState([]);
//   useEffect(()=>{
//     fetch('db.json'
//     ,{headers : { 
//       'Content-Type': 'application/json',
//       'Accept': 'application/json'
//             }
//     }
//     )
//       .then(res => res.json())
//         .then(
//           (result) => {
//             console.log(result);
//             setDatas(result);
//           });
    // .then(function(response){
    //   console.log(response)
      

    // })
    // .then(function(myJson) {
    //   console.log(myJson);
    //   setDatas(myJson);
    // });
  
  // },[])
 


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {/* <App /> */}
    <Router>
      <Header/>
      <Routes>  
        <Route exact path="/" element={<Home />}></Route>
        <Route path="/Propos" element={<Propos/>}></Route>
        <Route path="/NosVoyages" element={<NosVoyages/>}></Route>
        <Route
              path="/NosVoyages/:voyageSelectionne"
              element={<GetDatas />}
            ></Route>
        <Route path="/Contact" element={<ContactForm/>}></Route>
        </Routes>
      <Footer/>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
// }
// export default GetData;
// <GetData></GetData>