import { useState } from "react";

function ContactForm(){
    const[message,setMessage] = useState([]);
    const[txtMsg,setTxtMsg] = useState("");
    const[nomUser, setNomUser] = useState("");
    const[email, setEmail] = useState("");
  

    const handleChange = (event) =>{
        setTxtMsg(event.target.value);
    };

    const handleSubmit= (event) =>{
        event.preventDefault();
        const newMessage = {id:Date.now(), user: nomUser, mail: email, message: txtMsg }
        //on rajoute le nouveau message dans le tableau message
        const addMessage = (newMessage) => {
            const updatedMessage = [...message, newMessage];
            setMessage(updatedMessage);
            setNomUser ="";
            setEmail="";
            setTxtMsg="";
          };
    }
    return(
<form onSubmit={handleSubmit}>
    <label>Nom</label><br/>
    <input type="text" value={nomUser} required placeholder="Entrez votre nom" ></input><br/>
    <label>Email</label><br/>
    <input type="email" value={email} placeholder="nom@gmail.com" required></input><br/>
    <label>Votre message</label><br/>
    <textarea onChange={handleChange} class="input" name="txtMsg" rows="7" cols="30"></textarea><br/>
    <input id="submit_button" type="submit"></input>
</form>
    )
}
export default ContactForm;