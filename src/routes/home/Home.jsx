import { Link } from "react-router-dom";
function Home(){
    return(
        <div className="home" id="home">
		<div className="home-text">
			<h1>Travigo  <br/>Voyages</h1>
			<p>Explorez nos excursions et vivez la belle vie avec Travigo <br/>Des excursions qui vous donnent un  coup de foudre du monde .</p>
			<Link to="/" className="home-btn">Let's go now</Link>
		</div>
	</div>
    )
}

export default Home;