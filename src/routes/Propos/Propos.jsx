import bateau from './tourism.jpg';

function Propos(){
return(
    <section className="container">
		<div className="row-items">
			<div className="container">		
				<div className="title">
					<h2>Qui sommes-nous?</h2>
				</div>
				
				<p>Crée en 2005 par trois amis à Düsseldorf (Allemagne), trivago est depuis devenu un site leader de la recherche globale de logements . Notre focus est de modifier le mode de recherche et de comparaison d'hôtels et autres logement de millions de voyageurs. Faisant parti du groupe Expedia (NASDAQ: TRVG), la mission de trivago est de devenir votre compagnon lors de vos explorations du monde . </p>
			</div>
			<div className="container-img-b">
				<img src= {bateau} alt="bateau"/>
			</div>
		</div>
	</section>
)
}
export default Propos;