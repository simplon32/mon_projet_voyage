import { Link } from "react-router-dom";
import VoyageCard from "../../components/Voyage";
import { useState } from "react";
import { BrowserRouter as Router} from 'react-router-dom';


function NosVoyages(){
 
        const [voyages, setVoyage] = useState([
           {id: 1, 
            intitule: "A la découverte de l'himalaya", 
           photoUrl:"./himalaya.jpg", 
           pays: "Népal", 
           ville:"Katmandou",
           duree: "10jours", 
           detail:"Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellendus saepe, maxime dolore vero atque reprehenderit aspernatur dignissimos ducimus earum nam esse, rerum voluptas, facere eos?"
           },
           {
            id: 2, 
            intitule: "Coquillages et crustacés",
            photoUrl:"",
            pays: "îles Canaries",
            ville: "Santa Cruz de Tenerife",
            durée:"11jours",
            detail:"Découvrez les îles sauvages des canaries et ses nombreuses plages"
            }
           ]);

   
        // const voyageaajouter = {id, intitule, photoUrl, pays, duree, detail };
           return(    
                <div className="cartes">
                    {voyages.map((voyage, index) =>(   
                        // <div key ="{voyage.id}">     
                        <VoyageCard key={index} data={ voyage } />
                        // </div>
                        ))
                    }
               </div> 
                
           );
       
}

export default NosVoyages;