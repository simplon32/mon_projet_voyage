import React, { Component } from 'react';

class Logo extends Component {
    render() {
        return (
            <div>
                <div className="logo">
                    <img src="./logo.svg" alt="logo"/>
                </div>
            </div>
        );
    }
}

export default Logo;


