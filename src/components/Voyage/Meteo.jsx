import { useEffect, useState } from "react";
import VoyageCard from ".";

function Meteo(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [meteoPays, setMeteoPays] = useState([]);
    const [pays, setPays] = useState(props.pays);
    const urlStart = "https://api.openweathermap.org/data/2.5/weather?q=";
    const paramsUrl = "&$units=metric$lang=fr";
    const apiKey = "f94b443f99630b57c01a01b6b7a29c78";
    // Remarque : le tableau vide de dépendances [] indique
    // que useEffect ne s’exécutera qu’une fois, un peu comme
    // componentDidMount()
    useEffect(() => {
      fetch(`${urlStart}${pays}&appid=${apiKey}${paramsUrl}`)
        .then(res => res.json())
        .then(
          (result) => {
            setIsLoaded(true);

            const {main, name, weather} = result;
            var temp=main.temp;
            var climat = weather[0].description;
            console.log(temp,climat);
            setMeteoPays( {
              name: name,
              temp: temp,
              climat: climat
            });
           
           
          },
          // Remarque : il faut gérer les erreurs ici plutôt que dans
          // un bloc catch() afin que nous n’avalions pas les exceptions
          // dues à de véritables bugs dans les composants.
          (error) => {
            setIsLoaded(true);
            setError(error);
          }
        )     
    }, [pays])

    if (error) {
      return <div>Erreur : {error.message}</div>;
    }
    else if (!isLoaded) {
      return <div>Chargement...</div>;
    } 
    else {
      console.log(meteoPays);
      return(
        <div classname="meteo" >
          <h2 className="city-name" >{meteoPays.name}</h2>
          <div className="city-temp">la température est de: {meteoPays.temp}<sup>°C</sup></div> 
          le ciel est: {meteoPays.climat}                         
        </div>  
                   
        );      
    }
}
  export default Meteo;