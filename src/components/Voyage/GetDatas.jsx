import { useEffect, useState } from "react";
import VoyageDetaille from "./VoyageDetaille";

function GetDatas(){
  const [datas,setDatas]=useState([]);
  useEffect(()=>{
    fetch('../db.json'
    ,{headers : { 
      'Content-Type': 'application/json',
      'Accept': 'application/json'
            }
    }
    )
      .then(res => res.json())
        .then(
          (result) => {
            console.log(result);
            setDatas(result);
          });
    },[])
   return  <VoyageDetaille datas={datas}/>;
}
export default GetDatas;