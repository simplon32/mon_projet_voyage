import { useState } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router";
import VoyageDetaille from "./VoyageDetaille";

const VoyageCard =({data}) =>{

    return (
                <div>
                <Link to={`/NosVoyages/${data.id}`} >
                <div className="card p-2">
                <h2 className ="card-title">{data.intitule}</h2>
                <h4 className="card-subtitle mb-2 text-muted">{data.pays}</h4>
                <img className="rounded" src={data.photoUrl} alt="paysage"/>
                <h4 className="card-subtitle mb-2 text-muted">{data.duree}</h4>
                    </div>
                </Link>  
            </div>  

    );

}
export default VoyageCard;