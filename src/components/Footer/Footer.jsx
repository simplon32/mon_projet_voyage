
import { Link } from "react-router-dom";
function Footer(){
return(
    <section id="contact">
		<div className="footer">
			<div className="main">
				<div className="list">
					<h4>Liens raccourcis</h4>
					<ul>
						<li><Link to="/">A propos de nous</Link></li>
						<Link href="#">Termes & Conditions</Link>
						<Link href="#">Politique de confidentialité</Link>
						<Link href="#">Aide</Link>
						<Link href="#">Excursions</Link>
					</ul>
				</div>

				<div className="list">
					<h4>Support</h4>
					<ul>
						<li><Link href="#">A propos de nous</Link></li>
						<li><Link href="#">Termes & Conditions</Link></li>
						<li><Link href="#">Politique de confidentialité</Link></li>
						<li><Link href="#">Aide</Link></li>
						<li><Link href="#">Excursions</Link></li>
					</ul>
				</div>

				<div className="list">
					<h4>Informations pour nous contacter </h4>
					<ul>
						<li><Link href="#">98 West 21th Street</Link></li>
						<li><Link href="#">New York NY 10016</Link></li>
						<li><Link href="#">+(123)-123-1234</Link></li>
						<li><Link href="#">info@travigo.com</Link></li>
						<li><Link href="#">+(123)-123-1234</Link></li>
					</ul>
				</div>

				<div className="list">
					<h4>Connectons-nous</h4>
					<div className="social">
						<Link href="#"><i className='bx bxl-facebook' ></i></Link>
						<Link href="#"><i className='bx bxl-instagram-alt' ></i></Link>
						<Link href="#"><i className='bx bxl-twitter' ></i></Link>
						<Link href="#"><i className='bx bxl-linkedin' ></i></Link>
					</div>
				</div>
			</div>
		</div>

		<div className="end-text">
			<p>Copyright ©2022 All rights reserved | Travigo</p>
		</div>
	</section>

)
}

export default Footer;