import { Link } from "react-router-dom";
import Logo from "../logo/logo";

function Header(){
    return(
        <div className="d-flex" id="header">
            <Logo></Logo>
		    <div className="bx bx-menu" id="menu-icon"></div>
            <ul className="navbar">
                <Link  to = "/" >Accueil</Link>
                <Link className={(nav)=>(nav.isActive?"nav-active" : "")} to="/propos">à propos</Link>
                <Link className="m-1" to="/NosVoyages">Nos Voyages</Link>
                <Link className="m-1" to="/Contact">Contact</Link>
                {/* <li><a href="about.html">About</a></li>
                <li><a href="">Contact Us</a></li> */} 
           </ul>
        </div>
    )
}

export default Header;